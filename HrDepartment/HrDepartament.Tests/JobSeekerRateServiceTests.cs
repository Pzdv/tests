using HrDepartment.Application.Interfaces;
using HrDepartment.Application.Services;
using HrDepartment.Domain.Entities;
using HrDepartment.Domain.Enums;
using Moq;
using NUnit.Framework;
using System;

namespace HrDepartament.Tests
{
    [TestFixture]
    public class JobSeekerRateServiceTests
    {
        private ISanctionService _sanctionServiceStub;
        private JobSeeker _jobSeekerMock;
        private JobSeekerRateService _jobSeekerRateService;

        [SetUp]
        public void Setup()
        {
            _sanctionServiceStub = Mock.Of<ISanctionService>(
                x => x.IsInSanctionsListAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>()).Result == false);

            _jobSeekerRateService = new JobSeekerRateService(_sanctionServiceStub);
            _jobSeekerMock = Mock.Of<JobSeeker>();
            var birthDate = DateTime.Now.AddYears(-18);
            _jobSeekerMock.BirthDate = birthDate;
            _jobSeekerMock.Experience = 0;
            _jobSeekerMock.Education = EducationLevel.None;
            _jobSeekerMock.BadHabits = BadHabits.None;
        }

        [Test]
        public void JobSeekerRateServiceCtor_ParameterIsNull_ReturnArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new JobSeekerRateService(null));
        }

        [TestCase(25, 0)]
        [TestCase(30, 2)]
        [TestCase(35, 4)]
        [TestCase(45, 5)]
        [TestCase(60, 11)]
        [Test]
        public void CalculateBirthDateRating_TestAllExperience(int expected, int experience)
        {
            var actualRating = CalculateActualExperience(experience);

            Assert.AreEqual(expected, actualRating);
        }

        [Test]
        public void IsInSanctionsListAsync_IsIn_0Returned()
        {
            _sanctionServiceStub = Mock.Of<ISanctionService>(x => x.IsInSanctionsListAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>()).Result == true);
            _jobSeekerRateService = new JobSeekerRateService(_sanctionServiceStub);
            var actualRating = _jobSeekerRateService.CalculateJobSeekerRatingAsync(_jobSeekerMock).Result;

            Assert.AreEqual(0, actualRating);
        }

        [TestCase(17)]
        [TestCase(66)]
        [Test]
        public void CalculateBirthDateRating(int age)
        {
            _jobSeekerMock.BirthDate = DateTime.Now.AddYears(-age);

            var actualRating = _jobSeekerRateService.CalculateJobSeekerRatingAsync(_jobSeekerMock).Result;

            Assert.AreEqual(15, actualRating);
        }

        [TestCase(25, EducationLevel.None)]
        [TestCase(30, EducationLevel.School)]
        [TestCase(40, EducationLevel.College)]
        [TestCase(60, EducationLevel.University)]
        [Test]
        public void CalculateEducationRating_TestAllEducation(int expected, EducationLevel educationLevel)
        {
            var actualRating = CalculateActualRatingFromEducationLevel(educationLevel);

            Assert.AreEqual(expected, actualRating);
        }

        [TestCase(-1)]
        [TestCase(100)]
        [Test]
        public void CalculateEducationRating_InvalidEducation_ArgumentOutOfRangeReturned(int education)
        {
            _jobSeekerMock.Education = (EducationLevel)education;
            Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => _jobSeekerRateService.CalculateJobSeekerRatingAsync(_jobSeekerMock));
        }

        [TestCase(25, BadHabits.None)]
        [TestCase(20, BadHabits.Smoking)]
        [TestCase(10, BadHabits.Alcoholism)]
        [TestCase(0, BadHabits.Drugs)]
        [Test]
        public void CalculateHabitsRating_TestAllBadHabbits(int expected, BadHabits badHabbits)
        {
            var actualRating = CalculateActualRatingFromBadHabbits(badHabbits);

            Assert.AreEqual(expected, actualRating);
        }


        [Test]
        public void CalculateJobSeekerRatingAsyng_NegativeRating_0Returned()
        {
            // Arrange
            var jobSeeker = new Mock<JobSeeker>();
            jobSeeker.Object.BadHabits = BadHabits.Drugs;
            jobSeeker.Object.BirthDate = DateTime.Now.AddYears(-17);
            jobSeeker.Object.Education = EducationLevel.None;
            jobSeeker.Object.Experience = 0;

            // Act
            var actualRating = _jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker.Object).Result;

            // Assert
            Assert.AreEqual(0, actualRating);
        }

        private int CalculateActualRatingFromBadHabbits(BadHabits badHabbits)
        {
            _jobSeekerMock.BadHabits = badHabbits;
            return _jobSeekerRateService.CalculateJobSeekerRatingAsync(_jobSeekerMock).Result;
        }


        private int CalculateActualRatingFromEducationLevel(EducationLevel educationLevel)
        {
            _jobSeekerMock.Education = educationLevel;
            return _jobSeekerRateService.CalculateJobSeekerRatingAsync(_jobSeekerMock).Result;
        }

        private int CalculateActualExperience(int experience)
        {
            _jobSeekerMock.Experience = experience;
            return _jobSeekerRateService.CalculateJobSeekerRatingAsync(_jobSeekerMock).Result;
        }
    }
}