﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using HrDepartment.Application.Interfaces;
using HrDepartment.Application.Services;
using HrDepartment.Domain.Entities;
using HrDepartment.Infrastructure.Interfaces;
using Moq;
using NUnit.Framework;


namespace HrDepartament.Tests
{   
    [TestFixture]
    class JobSeekerServiceTests
    {
        [TestCase(100)]
        [TestCase(0)]
        [TestCase(5)]
        [Test]
        public void RateJobSeekerAsync(int fakeRating)
        {
            // Arrange
            var jobSeeker = new Mock<JobSeeker>();
            jobSeeker.Object.MiddleName = "middleName";

            var storage = Mock.Of<IStorage>(x => x.GetByIdAsync<JobSeeker, int>(It.IsAny<int>()).Result == jobSeeker.Object);

            var rateService = Mock.Of<IRateService<JobSeeker>>(x => x.CalculateJobSeekerRatingAsync(jobSeeker.Object).Result == fakeRating);

            var notificationService = new Mock<INotificationService>();
            notificationService.Setup(x => x.NotifyRockStarFoundAsync(jobSeeker.Object)).Returns(Task.Factory.StartNew(() => jobSeeker.Object.MiddleName = "RockStar"));

            var jobSeekerService = new JobSeekerService(storage, rateService, Mock.Of<IMapper>(), notificationService.Object);

            // Act
            var actualRating = jobSeekerService.RateJobSeekerAsync(It.IsAny<int>()).Result;

            // Assert
            if(actualRating == 100)
            {
                Assert.AreEqual("RockStar", jobSeeker.Object.MiddleName);
                Assert.AreEqual(fakeRating, actualRating);
            }
            else
                Assert.AreEqual(fakeRating, actualRating);
        }

    }
}
